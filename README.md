## Requirements

![Node: 8.14.0+](https://img.shields.io/badge/Node-8.14.0%2B-brightgreen.svg)<br/>
![Yarn: 1.12.3+](https://img.shields.io/badge/Yarn-1.12.3%2B-brightgreen.svg)

## Install

Simply install yarn packages and you're ready to start.
```bash
yarn install
```

## Run
The default run mode is Dev mode.  This provides watch functionality
and will rebuild and deploy automatically on file changes.
```bash
yarn start
```

TODO:  debug mode
