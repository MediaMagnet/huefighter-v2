import * as config from './config.json';
import * as delicious from './colors.json';
import * as Color from 'color';
import * as hue from 'node-hue-api';
import * as WebSocket from 'ws';
import {Prefix, Message} from "./message";
import chalk from 'chalk';

type LightState = {
  state: {
    rgb?: number[],
    on: boolean,
    bri: number,
    hue?: number,
    sat?: number,
    xy?: [number, number],
    ct?: number,
    alert: string,
    effect?: string,
    colormode?: string,
    reachable: boolean
  }
  type: string,
  name: string,
  modelid: string,
  manufacturername: string,
  uniqueid: string,
  swversion: string
}

function secureRandomColor(): string {
  return "#000000".replace(/0/g, () => (~~(Math.random() * 16)).toString(16)
  )
}

function randomLight(): number {
  return Math.floor(Math.random() * (+5 - +1)) + +1;
}

function displayResult(result: LightState | boolean) {
  //console.log(JSON.stringify(result, null, 2));
  console.log(result);
  if (typeof result === "object") {
    r = result["state"]["rgb"][0];
    g = result["state"]["rgb"][1];
    b = result["state"]["rgb"][2];
  }
  return true;
};

let HueApi = hue.HueApi,
  lightState = hue.lightState;
let n: number = 3;
let i: number = 0;
const wait = (ms: number) => new Promise(res => setTimeout(res, ms));
const user: string = config.user,
  host: string = config.bridge;
let api = new HueApi(host, user),
  r: number = 0,
  g: number = 0,
  b: number = 0;


async function partyMode(runTime: number, fixedColor: string) {
  while (i < runTime) {
    const hColor: string = secureRandomColor();
    const colorObj = Color(hColor).object();
    const state = lightState.create().on().rgb(colorObj['r'], colorObj['g'], colorObj['b']);
    const light = randomLight();
    api.setLightState(3, state);
    console.log(chalk.rgb(colorObj['r'], colorObj['g'], colorObj['b']).bold('Setting ' + light + ' to color ' + hColor));
    await wait(100);
    i++;
  }
  console.log("Partymode done \nSetting lights back to " + fixedColor);
  const fixColr = Color(fixedColor).object();
  const baseState = lightState.create().on().rgb(fixColr['r'], fixColr['g'], fixColr['b']);
  while (n < 5) {
    api.setLightState(3, baseState);
    console.log(n);
    await wait(250);
    n++;
  }
}

// partyMode(10, "#ffffff");

const ws = new WebSocket('wss://irc-ws.chat.twitch.tv');

ws.on('open', function open() {

  this.send('CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership');
  console.log("connected");
  if (config.talk === "enabled") {
    this.send('PASS ' + config.oauth);
    this.send('NICK ' + config.nick);
  } else {
    this.send('NICK justinfan' + Math.floor(Math.random() * 999999));
  }
  this.send('JOIN #' + config.channel);
  if (config.talk === "enabled") {
    // this.send("PRIVMSG #" + config.channel + " :.me HueFighter online, Let\'s do the thing!");
  }
});

ws.on('message', function (chatMsg: WebSocket.Data) {
  let msg = new Message(chatMsg.toString().replace(/\r|\n/gi, ''));
  if (msg.command === "PING") {
    ws.send(msg.raw.replace("PING", "PONG"));
  }
  if (msg.command === "PRIVMSG") {
    /*if (config.debug){
      console.log(msg.parameters[1]);
    }*/
    let badges = msg.tags.get("badges");
    if (badges && (badges.includes("broadcaster") || badges.includes("moderator"))) {
      const argv2 = msg.parameters[1].split(' ')[1];
      switch (msg.parameters[1].split(' ')[0]) {

        case "!partymode":
          partyMode(20, config.basecolor);
          break;
        case "!lightsoff":
          const lightOff = lightState.create().off();
          api.setLightState(3, lightOff);
          break;
        case "!lightson":
          const lightOn = lightState.create().on();
          api.setLightState(3, lightOn);
          break;
        case "!alert":
          const alertLight = lightState.create().alertLong();
          api.setLightState(3, alertLight);
          break;
        case "!colorforce":
          const setColor = Color(argv2).object();
          const colState = lightState.create().on().rgb(setColor['r'], setColor['g'], setColor['b']);
          api.setLightState(3, colState)
            .then(displayResult)
            .catch(console.error)
            .finally();
          break;
        case "!brightness":
          const lightBright = parseInt(argv2);
          const briState = lightState.create().brightness(lightBright);
          api.setLightState(3, briState)
            .then(displayResult)
            .catch(console.error)
            .finally();
          break;
        // add commands here
        default:
          break;
      }
    }
    let bits = msg.tags.get("bits");
    if (config.debug === true) {
      console.log(bits);
    }
    if (msg.parameters[1].startsWith("!getcolor")) {
      api.getLightStatusWithRGB(3)
        .then((result: LightState) => {
          //console.log(JSON.stringify(result, null, 2));
          console.log(result);
          if (typeof result === "object") {
            let r = result["state"]["rgb"][0];
            let g = result["state"]["rgb"][1];
            let b = result["state"]["rgb"][2];
            ws.send("PRIVMSG #" + config.channel +" :The Lights are a nice shade of " + `r:${r} g:${g} b:${b}`)
          }
          return true;
        })
        .catch(console.error)
        .finally();



    }
    if (bits) {
      let colorMatch: string = '';
      const colorString = msg.parameters[1].match(/(#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f])/);
      if (colorString) {
        colorMatch = colorString[0];
        if (config.debug === true) {
          console.log('HEX color is: ' + colorMatch);
        }
      } else {
        delicious["colors"].forEach((color) => {
          if (colorMatch === "" && msg.parameters[1].includes(color[0])) {
            colorMatch = color[1];
            console.log('Delicious color is: ' + colorMatch);
          }
        });
      }
      if (colorMatch !== '') {
        const colorParsed = Color(colorMatch).object();
        let interpValue: number = parseFloat(bits) / parseFloat(config.bitcap);
        if (interpValue > 1.0) {
          interpValue = 1.0
        }
        api.getLightStatusWithRGB(3)
          .then((result: LightState) => {
            //console.log(JSON.stringify(result, null, 2));
            console.log(result);
            if (typeof result === "object") {
              let r = result["state"]["rgb"][0],
                g = result["state"]["rgb"][1],
                b = result["state"]["rgb"][2],
                newRed = ((colorParsed["r"] - r) * interpValue),
                newGreen = ((colorParsed["g"] - g) * interpValue),
                newBlue = ((colorParsed["b"] - b) * interpValue);
              const bitState = lightState.create().rgb(newRed, newGreen, newBlue);
              api.setLightState(3, bitState)
            }
            return true;
          })
          .catch(console.error)
          .finally();

      }
    }
  }
});
