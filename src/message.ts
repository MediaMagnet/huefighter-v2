export class Prefix {
  realname: string;
  username: string;
  hostname: string;
  constructor(raw: string) {
    this.realname = this.username = this.hostname = "";
    if (raw.charAt(0) === ":") {
      let raw2 = raw.slice(1).split(/[!@]/);
      this.realname = raw2[0];
      this.username = raw2[1];
      this.hostname = raw2[2];
    }
  }
}

export class Message {
  raw: string;
  tags: Map<string, string>;
  command: string;
  prefix: Prefix;
  parameters: string[];
  constructor(raw: string) {
    this.raw = raw;
    this.tags = new Map;
    this.parameters = [];
    let data = raw.split(" ");
    let data2  = data.shift();
    let a = data2.charAt(0);
    if (a === "@") {
      data2.slice(1).split(";").forEach((r_tag) => {
        let s_tag = r_tag.split("=", 2);
        this.tags.set(s_tag[0], s_tag[1]);
      });
      data2  = data.shift();
      a = data2.charAt(0);
    }
    if (a === ":") {
      this.prefix = new Prefix(data2)
      data2  = data.shift();
      a = data2.charAt(0);
    }
    this.command = data2;
    for (;;) {
      if (data[0] == undefined) {
        break;
      } else if (data[0].charAt(0) === ":" || data.length === 1) {
        let parameter = data.join(" ").slice(1);
        this.parameters.push(parameter);
        break;
      } else {
        let parameter = data.shift();
        this.parameters.push(parameter);
      }
    }
  }
}